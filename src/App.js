import React, {Component} from 'react';
import {Layout, notification} from 'antd';
import './styles/app.css';
import WrappedLogin from "./components/login/WrappedLogin";
import SiderAdmin from "./components/sider/SiderAdmin";
import HeaderAdmin from "./components/header/HeaderAdmin";
import ContentAdmin from "./components/content/ContentAdmin";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {fetchToken} from "./duck/action/service/sessionActions";


class App extends Component {


    route = (logged) => {
        if (!logged) {
            return (<Layout id={"admin-holder"}>
                    <WrappedLogin/>
                </Layout>
            );
        } else {
            return (<Layout id={"admin-holder"}>
                <SiderAdmin/>
                <Layout>
                    <HeaderAdmin/>
                    <ContentAdmin/>
                </Layout>
            </Layout>);
        }
    };

    componentDidMount() {
        notification.open({
            message: 'Cookies',
            description: 'This website uses cookies for authentication',
        });

        this.props.fetchToken();
    }

    render() {
        return (
            <div>
                {this.route(this.props.logged)}
            </div>
        );
    }
}

App.propTypes = {
    logged: PropTypes.bool.isRequired,
    fetchToken: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    logged: state.session.logged,
});

export default connect(mapStateToProps, {fetchToken})(App);

