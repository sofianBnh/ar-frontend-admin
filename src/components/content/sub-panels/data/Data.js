import React, {Component} from 'react';
import {Alert, Divider, Layout} from "antd";
import TableData from "./sub-components/table/TableData";
import MenuData from "./sub-components/MenuData";
import {ADD, DATA_MENU} from "../../../../utils/consts";
import EditData from "./sub-components/edit/EditData";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {columnMapper} from "../../../../registery/mapper";
import {
    changeDataCurrentType,
    changeModalOperation,
    resetDataError,
    toggleModal
} from "../../../../duck/action/ui/dataActions";
import {fetchData} from "../../../../duck/action/service/contentActions";
import DataButtons from "./sub-components/table/table-holders/DataButtons";

const {Sider, Content} = Layout;

class Data extends Component {

    state = {
        currentData: [],
        columns: [],
        filteredInfo: null,
        sortedInfo: null,
    };

    //============================================================

    handleReload = (e) => {
        e.preventDefault();
        this.props.fetchData(this.props.activeDataContentKey);
    };

    handleAdd = (e) => {
        e.preventDefault();
        this.props.changeModalOperation(ADD);
        this.props.toggleModal()
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
        const currentColumn = columnMapper(this.props.activeDataContentKey,
            this.state.sortedInfo, this.state.filteredInfo);

        this.setState({
            columns: currentColumn
        });

    };

    //============================================================

    changeTableContent = ({key}) => {
        this.props.changeDataCurrentType(key);
    };

    displayTable = () => {
        if (this.props.activeDataContentKey && this.state.columns) {
            return (<div>

                {this.props.error && <Alert
                    style={{marginBottom: '1.5em'}}
                    message={this.props.error}
                    type="error"
                    showIcon
                />}

                <DataButtons
                    loading={this.props.loading}
                    onReload={this.handleReload}
                    onAdd={this.handleAdd}
                />

                <TableData
                    loading={this.props.loading}
                    columns={this.state.columns}
                    data={this.state.currentData}
                    onChange={this.handleChange}
                />

            </div>);
        } else {
            return (<div style={{textAlign: 'center'}}>
                <p>No data selected</p>
                <Divider/>
            </div>)
        }
    };

    //============================================================

    componentWillReceiveProps(nextProps) {

        if (nextProps.activeDataContentKey && nextProps.activeDataContentKey
            !== this.props.activeDataContentKey) {

            this.props.resetDataError();

            const currentTypeData = nextProps.content[nextProps.activeDataContentKey];

            if (!currentTypeData || currentTypeData.length === 0)
                this.props.fetchData(nextProps.activeDataContentKey);

            const currentColumn = columnMapper(nextProps.activeDataContentKey, this.state.sortedInfo
                , this.state.filteredInfo);

            this.setState({
                filteredInfo: null,
                sortedInfo: null,
                currentData: currentTypeData,
                columns: currentColumn
            });

        }

        if (nextProps.content && nextProps.content !== this.props.content) {

            this.props.resetDataError();

            const currentTypeData = nextProps.content[nextProps.activeDataContentKey];

            this.setState({
                currentData: currentTypeData,
            });
        }
    }

    //============================================================

    render() {
        return (
            <div>
                <h2>Database</h2>

                <EditData/>

                <Layout style={{padding: '2em 0', background: '#fff'}}>

                    <Sider width={200} style={{background: '#fff'}}>
                        <MenuData
                            activeDataContentKey={this.props.activeDataContentKey}
                            onSelect={this.changeTableContent}
                            menu={DATA_MENU}
                        />
                    </Sider>

                    <Content style={{padding: '0 2em', minHeight: 280}}>
                        {this.displayTable()}
                    </Content>

                </Layout>
            </div>
        );
    }
}

Data.propTypes = {
    changeDataCurrentType: PropTypes.func.isRequired,
    fetchData: PropTypes.func.isRequired,
    resetDataError: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,

    activeDataContentKey: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    content: PropTypes.object.isRequired,
    error: PropTypes.string,
};

const mapStateToProps = state => ({
    activeDataContentKey: state.data.activeDataContentKey,
    loading: state.data.loading,
    error: state.data.error,
    content: state.content,
});

export default connect(mapStateToProps, {
    changeDataCurrentType,
    fetchData,
    resetDataError,
    toggleModal,
    changeModalOperation
})(Data);
