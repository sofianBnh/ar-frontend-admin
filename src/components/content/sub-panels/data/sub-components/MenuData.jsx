import React, {Component} from 'react';
import {Menu} from "antd";
import {buildMenu} from "../../../../../utils/menus";
import PropTypes from 'prop-types';
import _ from 'lodash';

class MenuData extends Component {

    rootSubmenuKeys = [];
    state = {
        openKeys: [],
    };

    handleOpenChange = (openKeys) => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({openKeys});
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        }
    };

    componentDidMount() {
        this.rootSubmenuKeys = _.map(this.props.menu.sub, 'key');
    }

    render() {
        return (
            <Menu
                onOpenChange={this.handleOpenChange}
                onSelect={this.props.onSelect}
                openKeys={this.state.openKeys}
                style={{height: '100%'}}
                mode="inline"
            >
                {this.props.menu && this.props.menu.sub.map(subMenu => {
                    return buildMenu(subMenu)
                })}
            </Menu>
        );
    }
}


MenuData.propTypes = {
    menu: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    activeDataContentKey: PropTypes.string.isRequired,
};


export default MenuData;
