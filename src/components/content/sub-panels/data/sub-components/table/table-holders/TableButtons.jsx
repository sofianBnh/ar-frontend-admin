import {deleteData} from "../../../../../../../duck/action/service/contentActions";
import {changeCurrentEdit, changeModalOperation, toggleModal} from "../../../../../../../duck/action/ui/dataActions";
import {connect} from 'react-redux';
import PropTypes from "prop-types";
import {Button} from "antd";
import React from "react";
import {EDIT} from "../../../../../../../utils/consts";

const TableButtons = ({record, type, deleteData, changeCurrentEdit, toggleModal, changeModalOperation}) => {

    const deleteOp = () => {
        deleteData(type, record);
    };

    const editOp = () => {
        changeCurrentEdit(record);
        changeModalOperation(EDIT);
        toggleModal();
    };

    return (
        <div
            className={"table-operations"}>
            <Button shape={"circle"} icon={"edit"} onClick={editOp}/>
            <Button shape={"circle"} icon={"delete"} type={"danger"} onClick={deleteOp}/>
        </div>
    )
};


TableButtons.propTypes = {
    record: PropTypes.object.isRequired,
    type: PropTypes.string.isRequired,
    changeCurrentEdit: PropTypes.func.isRequired,
    deleteData: PropTypes.func.isRequired,
};

export default connect(null, {deleteData, changeCurrentEdit, toggleModal, changeModalOperation})(TableButtons);