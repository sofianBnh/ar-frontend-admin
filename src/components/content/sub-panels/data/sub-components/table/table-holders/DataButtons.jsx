import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "antd";


const DataButtons = ({loading, onReload, onAdd}) => {
    return (

        <div style={{marginBottom: '1.4em'}}>
            <Button
                icon={"sync"}
                type={"primary"}
                onClick={onReload}
                loading={loading}
            > Reload</Button>
            <Button
                icon={"plus-circle-o"}
                type={"primary"}
                style={{marginLeft: '1em'}}
                onClick={onAdd}
                loading={loading}
            > Add</Button>
        </div>

    );
};

DataButtons.propTypes = {
    loading: PropTypes.bool.isRequired,
    onReload: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
};

export default DataButtons;
