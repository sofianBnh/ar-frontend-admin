import React, {Component} from 'react';
import {Table} from 'antd';
import {getIdForEmbedded} from "../../../../../../utils/parser";
import PropTypes from 'prop-types';


class TableData extends Component {

    render() {
        return (
            <div>
                <Table
                    loading={this.props.loading}
                    columns={this.props.columns}
                    dataSource={this.props.data}
                    onChange={this.props.onChange}
                    size={'middle'}
                    rowKey={(record) => getIdForEmbedded(record)}
                />
            </div>
        );
    }
}

TableData.propTypes = {
    onChange: PropTypes.func.isRequired,
    columns: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    data: PropTypes.array,
};

export default TableData;
