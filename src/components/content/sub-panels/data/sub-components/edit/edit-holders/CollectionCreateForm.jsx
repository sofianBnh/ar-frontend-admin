import {Form, Modal} from "antd";
import React from 'react';
import {connect} from 'react-redux'
import {formMapper} from "../../../../../../../registery/mapper";
import _ from 'lodash';
import {buildSelectForm} from "../../../../../../../registery/dataForms";
import {fetchData} from "../../../../../../../duck/action/service/contentActions";


const mapStateToProps = state => ({
    content: state.content,
    activeDataContentKey: state.data.activeDataContentKey,
});

const CollectionCreateForm = Form.create({
    mapPropsToFields(props) {
        let initial = {};
        for (let key of Object.keys(props.fields)) {
            initial[key] = Form.createFormField({
                ...props.fields[key],
                value: props.fields[key].value,
            })
        }
        return initial;
    },
})(
    connect(mapStateToProps, {fetchData})(
        class extends React.Component {

            state = {
                formContent: 'loading',
                selects: [],
                tried: false,
            };

            makeSelects = () => {
                let selects = this.state.formContent.selects;
                let dec = this.props.form.getFieldDecorator;

                let results = [];
                _.forEach(selects, (select, index) => {
                    results.push(
                        <span key={index}>
                            {buildSelectForm(dec, select, this.props.content)}
                        </span>
                    );
                });

                this.setState({selects: results});
            };

            componentWillReceiveProps(nextProps) {

                if (nextProps.activeDataContentKey &&
                    this.props.activeDataContentKey !== nextProps.activeDataContentKey) {
                    nextProps.form.setFields({});
                    this.setState({tried: false});
                }

                if (nextProps.activeDataContentKey) {
                    let form = formMapper(nextProps.activeDataContentKey,
                        this.props.form.getFieldDecorator);

                    this.setState({formContent: form});

                    _.forEach(form.selects, select => {
                        if (this.props.content[select.data].length === 0 && !this.state.tried) {
                            this.props.fetchData(select.data);
                        }
                    });

                    this.setState({tried: true});
                    this.makeSelects();
                }

                if (nextProps.content !== this.props.content) {
                    this.makeSelects();
                }

            }

            render() {
                const {visible, onCancel, onCreate} = this.props;
                return (
                    <Modal
                        visible={visible}
                        title="Create a new collection"
                        okText="Create"
                        onCancel={onCancel}
                        onOk={onCreate}
                    >
                        <div id={"edit"}>
                            <Form>
                                {this.state.selects.map(result => result)}
                                {this.state.formContent.fields && this.state.formContent.fields.map(field => field)}
                            </Form>
                        </div>
                    </Modal>
                );
            };
        }
    )
);

export default CollectionCreateForm;