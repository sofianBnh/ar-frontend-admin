import React from "react";
import PropTypes from 'prop-types';
import CollectionCreateForm from "./edit-holders/CollectionCreateForm";
import {connect} from 'react-redux'
import {toggleModal} from "../../../../../../duck/action/ui/dataActions";
import {patchData, postData} from "../../../../../../duck/action/service/contentActions";
import {ADD, EDIT} from "../../../../../../utils/consts";
import moment from "moment";


const buildFormProps = (old) => {
    let result = {};

    for (let key of Object.keys(old)) {

        if (key in old._links && old[key]._links) {
            result[key] = old[key]._links.self.href
        } else {
            let tempDate = Date.parse(old[key]);
            if (isNaN(tempDate))
                result[key] = {value: old[key]};
            else {
                result[key] = {value: moment(old[key])};
            }
        }
    }
    return result;
};

class ContentEdition extends React.Component {


    state = {
        fields: {},
    };

    handleCancel = () => {
        const form = this.formRef.props.form;
        form.resetFields();
        this.props.toggleModal();
    };

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields(this.create);
    };

    create = (err, values) => {
        const form = this.formRef.props.form;

        if (err) {
            console.log(err);
            return;
        }
        this.handleOperation(values);
        form.resetFields();
        this.props.toggleModal();
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleOperation = (data) => {
        if (this.props.operation === EDIT) {
            this.props.patchData(this.props.activeDataContentKey, data, this.props.currentEdit._links.self.href)
        } else if (this.props.operation === ADD) {
            this.props.postData(this.props.activeDataContentKey, data);
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.activeDataContentKey && this.props.activeDataContentKey !== nextProps.activeDataContentKey) {
            const form = this.formRef.props.form;
            form.resetFields();
            this.setState({fields: {}})
        }
        if (nextProps.currentEdit && nextProps.currentEdit !== this.props.currentEdit) {
            this.setState({
                fields: {
                    ...buildFormProps(nextProps.currentEdit)
                }
            });
        }
        if (nextProps.operation && nextProps.operation !== this.props.operation) {
            if (nextProps.operation === ADD) {
                this.setState({fields: {}})
            }
        }
    }

    render() {
        const fields = this.state.fields;
        return (
            <div>
                <CollectionCreateForm
                    fields={fields}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.props.editOpen}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
            </div>
        );
    }
}

ContentEdition.propTypes = {
    visible: PropTypes.bool.isRequired,
    activeDataContentKey: PropTypes.string.isRequired,
    editOpen: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    postData: PropTypes.func.isRequired,
    operation: PropTypes.string.isRequired,
    currentEdit: PropTypes.object,
};

const mapStateToProps = state => ({
    visible: state.data.editOpen,
    activeDataContentKey: state.data.activeDataContentKey,
    editOpen: state.data.editOpen,
    operation: state.data.editOperation,
    currentEdit: state.data.currentEdit,
});

export default connect(mapStateToProps, {toggleModal, postData, patchData})(ContentEdition);

