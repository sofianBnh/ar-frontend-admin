import React, {Component} from 'react';
import {Layout} from "antd";
import {contentMapper} from "../../registery/mapper";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import '../../styles/content.css';

const {Content} = Layout;

class ContentAdmin extends Component {
    render() {
        return (
            <Content style={{margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280}}>
                {contentMapper(this.props.siderActiveKey)}
            </Content>
        );
    }
}

ContentAdmin.propTypes = {
    siderActiveKey: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
    siderActiveKey: state.ui.siderActiveKey,
});

export default connect(mapStateToProps, {})(ContentAdmin);
