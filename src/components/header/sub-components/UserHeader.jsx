import React from 'react';
import PropTypes from 'prop-types';
import {Button, Dropdown, Icon, Menu} from "antd";

const LOGOUT_ACTION_KEY = 'LOGOUT_ACTION';

const UserHeader = ({username, logout}) => {

    function handleMenuClick(e) {
        if (e.key === LOGOUT_ACTION_KEY) {
            logout();
        }
    }

    const menu = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item key={LOGOUT_ACTION_KEY}>
                <Icon type={"logout"} style={{marginRight: 20}}/>
                Logout
            </Menu.Item>
        </Menu>
    );

    return (
        <span id={"header-user"}>

            <Dropdown overlay={menu} placement="bottomLeft">
                <Button style={{marginRight: 8}}
                        shape="circle" icon="setting"
                />
            </Dropdown>

            <span>{username}</span>

        </span>
    );
};


UserHeader.propTypes = {
    username: PropTypes.string.isRequired,
    logout: PropTypes.func.isRequired,
};

export default UserHeader;
