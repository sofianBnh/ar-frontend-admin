import React from 'react';
import {Icon} from "antd";
import PropTypes from 'prop-types';


const ToggleHeader = ({toggle, collapsed}) => {
    return (
        <div>
            <Icon
                className="trigger"
                onClick={toggle}
                type={collapsed ? 'menu-unfold' : 'menu-fold'}
            />
        </div>
    );
};

ToggleHeader.propTypes = {
    toggle: PropTypes.func.isRequired,
    collapsed: PropTypes.bool.isRequired,
};


export default ToggleHeader;
