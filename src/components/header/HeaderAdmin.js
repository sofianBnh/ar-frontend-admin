import React, {Component} from 'react';
import {Col, Layout, Row} from "antd";
import PropTypes from 'prop-types';
import UserHeader from "./sub-components/UserHeader";
import ToggleHeader from "./sub-components/ToggleHeader";
import '../../styles/header.css'
import {toggleSider} from "../../duck/action/ui/uiActions";
import {connect} from 'react-redux';
import {logout} from "../../duck/action/service/sessionActions";

const {Header} = Layout;

class HeaderAdmin extends Component {

    toggle = () => {
        this.props.toggleSider();
    };

    render() {
        return (
            <Header style={{background: '#fff', padding: 0}}>

                <Row>
                    <Col lg={4} md={4} sm={8} xs={8}>
                        <ToggleHeader collapsed={!this.props.siderOpen} toggle={this.toggle}/>
                    </Col>

                    <Col lg={16} md={16} sm={8} xs={8}/>

                    <Col lg={4} md={4} sm={8} xs={8}>
                        <UserHeader username={this.props.username} logout={this.props.logout}/>
                    </Col>
                </Row>

            </Header>
        );
    }
}

HeaderAdmin.propTypes = {
    siderOpen: PropTypes.bool.isRequired,
    username: PropTypes.string.isRequired,

    toggleSider: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    siderOpen: state.ui.siderOpen,
    username: state.session.username,
});

export default connect(mapStateToProps, {toggleSider, logout})(HeaderAdmin);
