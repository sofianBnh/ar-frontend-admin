import {Alert, Card, Divider, Form} from 'antd';
import React from "react";
import '../../styles/login.css';
import {RESTAURANT_NAME} from "../../utils/consts";
import NormalLoginForm from "./sub-components/NormalLoginForm";
import Spin from "antd/es/spin/index";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';


const WrappedLogin = ({sending, error}) => {

    const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

    const displayLogin = (sending, error) => {
        if (sending) {
            return <div>
                <Spin style={{display: 'block', margin: '5vh auto'}} size={"large"}/>
            </div>
        } else {
            return <div>
                <div className={"login-logo"}>
                    <h2>{RESTAURANT_NAME}</h2>
                    <div className={"login-sub-header"}><Divider>Administration</Divider></div>
                </div>
                <WrappedNormalLoginForm/>
                {error && <Alert message={error} type="error" showIcon/>}
            </div>;
        }
    };

    return <div className={"login-holder"} style={{margin: '25vh auto'}}>

        <Card title="Login" bordered={false} style={{width: "20vw", minWidth: "350px"}}>
            {displayLogin(sending, error)}
        </Card>
    </div>
};

WrappedLogin.propTypes = {
    error: PropTypes.string,
};

const mapStateToProps = state => ({
    error: state.session.error,
    sending: state.session.sending
});

export default connect(mapStateToProps, {})(WrappedLogin);
