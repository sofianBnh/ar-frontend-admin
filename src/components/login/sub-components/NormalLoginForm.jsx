import React from "react";
import {Button, Form, Icon, Input} from 'antd';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {login, resetLoginError} from "../../../duck/action/service/sessionActions";


const FormItem = Form.Item;

class NormalLoginForm extends React.Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            this.props.resetLoginError();
            if (!err) {
                this.props.login(values);
            }
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">

                <FormItem>
                    {getFieldDecorator('username', {
                        rules: [{required: true, message: 'Please input your username!'}],
                    })(
                        <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                               placeholder="Username"/>
                    )}
                </FormItem>

                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Please input your Password!'}],
                    })(
                        <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                               type="password"
                               placeholder="Password"/>
                    )}
                </FormItem>

                <FormItem>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                </FormItem>

            </Form>
        );
    }
}

NormalLoginForm.propTypes = {
    login: PropTypes.func.isRequired,
};


export default connect(null, {login, resetLoginError})(NormalLoginForm);