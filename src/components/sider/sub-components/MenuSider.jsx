import React from 'react';
import {Icon, Menu} from "antd";
import {CHART_MENU, DATA_MENU, DATA_MENU_KEY} from "../../../utils/consts";
import PropTypes from 'prop-types';

const data = DATA_MENU;
const chart = CHART_MENU;

const MenuHeader = ({onSelect}) => {

    return (
        <Menu
            theme="dark"
            defaultSelectedKeys={[DATA_MENU_KEY]}
            mode="inline"
            onSelect={onSelect}
        >
            <Menu.Item key={chart.key}>
                {chart.icon && <Icon type={chart.icon}/>}
                <span>{chart.name}</span>
            </Menu.Item>

            <Menu.Item key={data.key}>
                {data.icon && <Icon type={data.icon}/>}
                <span>{data.name}</span>
            </Menu.Item>

        </Menu>

    );
};

MenuHeader.propTypes = {
    onSelect: PropTypes.func.isRequired,
};

export default MenuHeader;
