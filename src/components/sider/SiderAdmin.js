import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout} from "antd";
import '../../styles/sider.css'
import MenuSider from "./sub-components/MenuSider";
import {connect} from 'react-redux';
import {changeSiderActiveKey} from "../../duck/action/ui/uiActions";

const {Sider} = Layout;

class SiderAdmin extends Component {

    changeContent = ({key}) => {
        this.props.changeSiderActiveKey(key);
    };

    render() {
        return (
            <Sider
                trigger={null}
                collapsible
                collapsed={!this.props.siderOpen}
            >
                <div className="logo"/>

                <MenuSider onSelect={this.changeContent}/>

            </Sider>
        );
    }
}

SiderAdmin.propTypes = {
    siderOpen: PropTypes.bool.isRequired,
    changeSiderActiveKey: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
    siderOpen: state.ui.siderOpen,
});

export default connect(mapStateToProps, {changeSiderActiveKey})(SiderAdmin);
