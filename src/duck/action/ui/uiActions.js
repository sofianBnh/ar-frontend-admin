import {CHANGE_CONTENT, TOGGLE_SIDER} from '../types';

export const toggleSider = () => dispatch => {
    dispatch({
        type: TOGGLE_SIDER,
    });
};

export const changeSiderActiveKey = (menuType) => dispatch => {
    dispatch({
        type: CHANGE_CONTENT,
        payload: menuType,
    });
};