import {
    CHANGE_CURRENT_EDIT,
    CHANGE_DATA_CONTENT,
    CHANGE_DATA_ERROR,
    CHANGE_EDIT_OPERATION,
    TOGGLE_DATA_LOADING,
    TOGGLE_EDIT_OPEN
} from "../types";

export const toggleModal = () => dispatch => {
    dispatch({
        type: TOGGLE_EDIT_OPEN,
    });
};

export const toggleDataLoading = () => dispatch => {
    dispatch({
        type: TOGGLE_DATA_LOADING,
    });
};

export const resetDataError = () => dispatch => {
    dispatch({
        type: CHANGE_DATA_ERROR,
        payload: '',
    });
};

export const changeDataCurrentType = (dataType) => dispatch => {
    dispatch({
        type: CHANGE_DATA_CONTENT,
        payload: dataType,
    });
};

export const changeCurrentEdit = (data) => dispatch => {
    dispatch({
        type: CHANGE_CURRENT_EDIT,
        payload: data,
    });
};

export const changeModalOperation = (operation) => dispatch => {
    dispatch({
        type: CHANGE_EDIT_OPERATION,
        payload: operation,
    })
};