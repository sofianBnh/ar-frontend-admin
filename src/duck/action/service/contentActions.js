import {getAuthHeader, getErrorFromResponse} from "../../../utils/requests";
import {CHANGE_DATA_ERROR, TOGGLE_DATA_LOADING, UPDATE_LIST_CONTENT} from "../types";
import axios from "axios/index";
import {routeMapper} from "../../../registery/mapper";
import {getIdForEmbedded} from "../../../utils/parser";

async function depthLoading(data, dataRoute, header) {

    if (!data)
        return [];

    let result = [];

    for (let entity of data) {

        let selfLink = entity._links.self.href;

        let newEntity = {
            ...entity
        };

        const links = entity._links;

        for (let key of  Object.keys(links)) {

            let link = links[key].href;

            if (link !== selfLink) {

                let index = link.toString().indexOf(dataRoute);
                link = link.toString().substring(index, link.length);

                let response = await axios.get(link, {
                    headers: header,
                });

                let lastSlashIndex = link.lastIndexOf('/');
                let type = link.substring(lastSlashIndex + 1, link.length);

                let resource = {};

                const embedded = response.data._embedded;

                if (!embedded)
                    resource[type] = response.data;
                else
                    resource = response.data._embedded;


                newEntity = {
                    ...newEntity,
                    ...resource,
                };
            }
        }
        result.push(newEntity);
    }

    return result;
}

const fetchAllWithDispatcher = (dataType, dispatch) => {

    dispatch({
        type: TOGGLE_DATA_LOADING,
    });

    const route = routeMapper(dataType);

    const header = getAuthHeader();

    const request = axios.get(route, {
        headers: header,
    });

    request.then(response => {

        let embedded = response.data._embedded;
        let baseData = [];
        for (let key of Object.keys(embedded)) {
            if (key !== '_links') {
                baseData = [...baseData, ...embedded[key]]
            }
        }


        let data = depthLoading(baseData, route, header);

        data.then(fullData => {

            dispatch({
                type: TOGGLE_DATA_LOADING,
            });

            dispatch({
                type: UPDATE_LIST_CONTENT,
                payload: {
                    dataType: dataType,
                    data: fullData,
                }
            });

        });


    }).catch(error => {

        dispatch({
            type: TOGGLE_DATA_LOADING,
        });

        dispatch({
            type: CHANGE_DATA_ERROR,
            payload: getErrorFromResponse(error),
        });

    });


};

export const fetchData = (dataType) => dispatch => {
    fetchAllWithDispatcher(dataType, dispatch);
};

export const postData = (dataType, data) => dispatch => {
    console.log('posting');
    const route = routeMapper(dataType);

    const request = axios.post(route, data, {
        headers: getAuthHeader(),
    });

    request.then(response => {
        fetchAllWithDispatcher(dataType, dispatch);
    }).catch(error => {
        dispatch({
            type: CHANGE_DATA_ERROR,
            payload: getErrorFromResponse(error),
        });
    });


};

export const patchData = (dataType, data, link) => dispatch => {

    const request = axios.patch(link, data, {
        headers: getAuthHeader(),
    });

    request.then(response => {
        fetchAllWithDispatcher(dataType, dispatch);
    }).catch(error => {
        dispatch({
            type: CHANGE_DATA_ERROR,
            payload: getErrorFromResponse(error),
        });

    });

};

export const deleteData = (dataType, data) => dispatch => {

    const route = routeMapper(dataType);

    const id = getIdForEmbedded(data);

    const request = axios.delete(route + "/" + id, {
        headers: getAuthHeader(),
    });

    request.then(response => {
        fetchAllWithDispatcher(dataType, dispatch);
    }).catch(error => {
        dispatch({
            type: CHANGE_DATA_ERROR,
            payload: getErrorFromResponse(error),
        });

    });

};

