import axios from 'axios';
import {ADMIN_CHECK_ROUTE, LOGIN_ROUTE} from "../../../utils/routes";
import * as cookies from "react-cookies";
import {TOKEN_BEARER, TOKEN_COOKIE_KEY} from "../../../utils/consts";
import {CHANGE_LOGIN_ERROR, LOGIN, LOGOUT, RESET_CONTENT, RESET_DATA, TOGGLE_SENDING} from "../types";
import {getErrorFromResponse} from "../../../utils/requests";
import jwt from 'jsonwebtoken';


export const fetchToken = () => dispatch => {

    const token = cookies.load(TOKEN_COOKIE_KEY);

    if (token && token !== '')
        dispatch({
            type: LOGIN,
            payload: jwt.decode(token).sub,
        });

};

export const login = ({username, password}) => dispatch => {

    dispatch({
        type: TOGGLE_SENDING,
    });

    const request = axios.post(LOGIN_ROUTE,
        {username: username, password: password}
    );

    request.then(response => {

        const token = response.data.token;

        if (token && token !== '') {
            checkAdmin(token, dispatch);
            return;
        }

        dispatch({
            type: TOGGLE_SENDING,
        });

        dispatch({
            type: CHANGE_LOGIN_ERROR,
            payload: 'Could not connect',
        });

    }).catch(error => {
        dispatch({
            type: CHANGE_LOGIN_ERROR,
            payload: getErrorFromResponse(error),
        });

        dispatch({
            type: TOGGLE_SENDING,
        });

    });

};


const checkAdmin = (token, dispatch) => {

    const headers = {
        Authorization: TOKEN_BEARER + token
    };

    const request = axios.get(ADMIN_CHECK_ROUTE, {headers: headers});

    request.then(response => {

        cookies.save(TOKEN_COOKIE_KEY, token);

        dispatch({type: RESET_DATA,});

        dispatch({type: RESET_CONTENT,});


        dispatch({
            type: LOGIN,
            payload: jwt.decode(token).sub,
        });

        dispatch({type: TOGGLE_SENDING,});


    }).catch(error => {
        dispatch({
            type: CHANGE_LOGIN_ERROR,
            payload: getErrorFromResponse(error),
        });
    });

};

export const resetLoginError = () => dispatch => {
    dispatch({
        type: CHANGE_LOGIN_ERROR,
        payload: '',
    })
};

export const logout = () => dispatch => {

    cookies.remove(TOKEN_COOKIE_KEY);

    dispatch({type: RESET_DATA,});

    dispatch({type: RESET_CONTENT,});

    dispatch({type: LOGOUT,});

};