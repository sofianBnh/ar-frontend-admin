import {CHANGE_LOGIN_ERROR, LOGIN, LOGOUT, TOGGLE_SENDING} from "../../action/types";

const INITIAL_STATE = {
    sending: false,
    logged: false,
    username: '',
    error: '',
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case LOGIN:
            return {
                ...state,
                logged: true,
                username: action.payload,
            };

        case LOGOUT:
            return {
                ...state,
                logged: false,
                username: '',
            };

        case CHANGE_LOGIN_ERROR:
            return {
                ...state,
                error: action.payload,
            };

        case TOGGLE_SENDING:
            return {
                ...state,
                sending: !state.sending,
            };

        default:
            return state;

    }
}