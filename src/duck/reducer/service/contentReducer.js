import {
    DEFAULT_DISCOUNT_DATA,
    DYNAMIC_DISCOUNT_DATA,
    FIXED_DISCOUNT_DATA,
    MEAL_DATA,
    PLANNING_DATA,
    RESERVATION_DATA,
    RESTAURANT_DATA,
    ROLE_DATA,
    SIMPLE_USER_DATA,
    SPECIAL_DAY_DATA,
    TABLE_DATA,
    USER_DATA
} from "../../../utils/consts";
import {RESET_CONTENT, UPDATE_LIST_CONTENT} from "../../action/types";

const INITIAL_STATE = {
    [RESERVATION_DATA]: [],
    [RESTAURANT_DATA]: [],
    [TABLE_DATA]: [],
    [MEAL_DATA]: [],
    [DEFAULT_DISCOUNT_DATA]: [],
    [DYNAMIC_DISCOUNT_DATA]: [],
    [FIXED_DISCOUNT_DATA]: [],
    [PLANNING_DATA]: [],
    [SPECIAL_DAY_DATA]: [],
    [USER_DATA]: [],
    [SIMPLE_USER_DATA]: [],
    [ROLE_DATA]: [],
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {


        case RESET_CONTENT:
            return {
                ...state,
                ...INITIAL_STATE,
            };

        case UPDATE_LIST_CONTENT:
            return {
                ...state,
                [action.payload.dataType]: action.payload.data,
            };

        default:
            return state;
    }
}