import {DATA_MENU_KEY} from "../../../utils/consts";
import {CHANGE_CONTENT, TOGGLE_SIDER} from "../../action/types";

const INITIAL_STATE = {
    siderOpen: false,
    siderActiveKey: DATA_MENU_KEY,
};


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case TOGGLE_SIDER:
            return {
                ...state,
                siderOpen: !state.siderOpen,
            };

        case CHANGE_CONTENT:
            return {
                ...state,
                siderActiveKey: action.payload,
            };

        default:
            return state;
    }
}