import {
    CHANGE_CURRENT_EDIT,
    CHANGE_DATA_CONTENT,
    CHANGE_DATA_ERROR,
    CHANGE_EDIT_OPERATION,
    RESET_DATA,
    TOGGLE_DATA_LOADING,
    TOGGLE_EDIT_OPEN
} from "../../action/types";

const INITIAL_STATE = {
    activeDataContentKey: '',
    currentEdit: '',
    editOpen: false,
    editOperation: '',
    loading: false,
    error: '',
};


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case CHANGE_EDIT_OPERATION:
            return {
                ...state,
                editOperation: action.payload,
            };

        case RESET_DATA:
            return {
                ...state,
                ...INITIAL_STATE,
            };

        case TOGGLE_DATA_LOADING:
            return {
                ...state,
                loading: !state.loading
            };

        case TOGGLE_EDIT_OPEN:
            return {
                ...state,
                editOpen: !state.editOpen,
            };

        case CHANGE_DATA_CONTENT:
            return {
                ...state,
                activeDataContentKey: action.payload,
            };

        case CHANGE_DATA_ERROR:
            return {
                ...state,
                error: action.payload,
            };

        case CHANGE_CURRENT_EDIT:
            return {
                ...state,
                currentEdit: action.payload,
            };

        default:
            return state;
    }

}
