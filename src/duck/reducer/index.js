import {combineReducers} from "redux";
import uiReducer from "./ui/uiReducer";
import dataReducer from "./ui/dataReducer";
import sessionReducer from "./service/sessionReducer";
import contentReducer from "./service/contentReducer";

export default combineReducers({
    content: contentReducer,
    session: sessionReducer,
    data: dataReducer,
    ui: uiReducer,
});