import {
    CHART_MENU_KEY,
    DATA_MENU_KEY,
    DEFAULT_DISCOUNT_DATA,
    DYNAMIC_DISCOUNT_DATA,
    FIXED_DISCOUNT_DATA,
    MEAL_DATA,
    PLANNING_DATA,
    RESERVATION_DATA,
    RESTAURANT_DATA,
    ROLE_DATA,
    SIMPLE_USER_DATA,
    SPECIAL_DAY_DATA,
    TABLE_DATA,
    USER_DATA
} from "../utils/consts";
import Data from "../components/content/sub-panels/data/Data";
import Chart from "../components/content/sub-panels/charts/Chart";
import React from "react";
import _ from 'lodash';
import {
    DEFAULT_DISCOUNT_COLUMNS,
    DYNAMIC_DISCOUNT_COLUMNS,
    FIXED_DISCOUNT_COLUMNS,
    MEAL_COLUMNS,
    PLANNING_COLUMNS,
    RESERVATION_COLUMNS,
    RESTAURANT_COLUMNS,
    ROLE_COLUMNS,
    SIMPLE_USER_COLUMNS,
    SPECIAL_DAY_COLUMNS,
    TABLE_COLUMNS,
    USER_COLUMNS
} from "./tableColums";
import {
    DEFAULT_DISCOUNT_ROUTE,
    DYNAMIC_DISCOUNT_ROUTE,
    FIXED_DISCOUNT_ROUTE,
    MEAL_ROUTE,
    PLANNING_ROUTE,
    RESERVATION_ROUTE,
    RESTAURANT_ROUTE,
    ROLE_ROUTE,
    SIMPLE_USER_ROUTE,
    SPECIAL_DAY_ROUTE,
    TABLE_ROUTE,
    USER_ROUTE
} from "../utils/routes";
import {
    defaultDiscountForm,
    dynamicDiscountForm,
    fixedDiscountForm,
    mealForm,
    planningForm,
    reservationForm,
    restaurantForm,
    roleForm,
    simpleUserForm,
    specialDayForm,
    tableForm,
    userForm
} from "./dataForms";

// map routes
export const routeMapper = (dataType) => {
    switch (dataType) {
        case  RESERVATION_DATA:
            return RESERVATION_ROUTE;
        case  DEFAULT_DISCOUNT_DATA:
            return DEFAULT_DISCOUNT_ROUTE;
        case  DYNAMIC_DISCOUNT_DATA:
            return DYNAMIC_DISCOUNT_ROUTE;
        case  FIXED_DISCOUNT_DATA:
            return FIXED_DISCOUNT_ROUTE;
        case  SPECIAL_DAY_DATA:
            return SPECIAL_DAY_ROUTE;
        case  PLANNING_DATA:
            return PLANNING_ROUTE;
        case  RESTAURANT_DATA:
            return RESTAURANT_ROUTE;
        case  TABLE_DATA:
            return TABLE_ROUTE;
        case  MEAL_DATA:
            return MEAL_ROUTE;
        case  SIMPLE_USER_DATA:
            return SIMPLE_USER_ROUTE;
        case  USER_DATA:
            return USER_ROUTE;
        case  ROLE_DATA:
            return ROLE_ROUTE;
        default:
            return '';
    }
};

// map columns
export const columnMapper = (dataType, sortedInfo, filteredInfo) => {
    let columns;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};

    switch (dataType) {
        case  RESERVATION_DATA:
            columns = RESERVATION_COLUMNS;
            break;
        case  DEFAULT_DISCOUNT_DATA:
            columns = DEFAULT_DISCOUNT_COLUMNS;
            break;
        case  DYNAMIC_DISCOUNT_DATA:
            columns = DYNAMIC_DISCOUNT_COLUMNS;
            break;
        case  FIXED_DISCOUNT_DATA:
            columns = FIXED_DISCOUNT_COLUMNS;
            break;
        case  SPECIAL_DAY_DATA:
            columns = SPECIAL_DAY_COLUMNS;
            break;
        case  PLANNING_DATA:
            columns = PLANNING_COLUMNS;
            break;
        case  RESTAURANT_DATA:
            columns = RESTAURANT_COLUMNS;
            break;
        case  TABLE_DATA:
            columns = TABLE_COLUMNS;
            break;
        case  MEAL_DATA:
            columns = MEAL_COLUMNS;
            break;
        case  SIMPLE_USER_DATA:
            columns = SIMPLE_USER_COLUMNS;
            break;
        case  USER_DATA:
            columns = USER_COLUMNS;
            break;
        case  ROLE_DATA:
            columns = ROLE_COLUMNS;
            break;
        default:
            columns = '';
            break;

    }

    let updatedColumn = [];

    _.forEach(columns, (col) => {
        let extras = {};
        if ('filters' in col) {
            extras['filteredValue'] = filteredInfo[col.dataIndex] || null;
        }

        let items = {
            ...col,
            sortOrder: sortedInfo.columnKey === col.dataIndex && sortedInfo.order,
            ...extras,
        };
        updatedColumn.push(items)
    });

    return updatedColumn;
};

// map forms
export function formMapper(dataType, getFieldDecorator) {

    switch (dataType) {
        case  RESERVATION_DATA:
            return reservationForm(getFieldDecorator);
        case  DEFAULT_DISCOUNT_DATA:
            return defaultDiscountForm(getFieldDecorator);
        case  DYNAMIC_DISCOUNT_DATA:
            return dynamicDiscountForm(getFieldDecorator);
        case  FIXED_DISCOUNT_DATA:
            return fixedDiscountForm(getFieldDecorator);
        case  SPECIAL_DAY_DATA:
            return specialDayForm(getFieldDecorator);
        case  PLANNING_DATA:
            return planningForm(getFieldDecorator);
        case  RESTAURANT_DATA:
            return restaurantForm(getFieldDecorator);
        case  TABLE_DATA:
            return tableForm(getFieldDecorator);
        case  MEAL_DATA:
            return mealForm(getFieldDecorator);
        case  SIMPLE_USER_DATA:
            return simpleUserForm(getFieldDecorator);
        case  USER_DATA:
            return userForm(getFieldDecorator);
        case  ROLE_DATA:
            return roleForm(getFieldDecorator);
        default:
            return 'Form Not found';
    }

}

// map contents
export const contentMapper = (menuType) => {
    switch (menuType) {

        case DATA_MENU_KEY:
            return <Data/>;

        case CHART_MENU_KEY:
            return <Chart/>;

        default:
            return <Data/>
    }
};



