import React from "react";
import {Icon} from "antd";
import moment from "moment";
import {
    DEFAULT_DISCOUNT_DATA,
    DYNAMIC_DISCOUNT_DATA,
    FIXED_DISCOUNT_DATA,
    MEAL_DATA,
    PLANNING_DATA,
    RESERVATION_DATA,
    RESTAURANT_DATA,
    ROLE_DATA,
    SIMPLE_USER_DATA,
    SPECIAL_DAY_DATA,
    TABLE_DATA,
    USER_DATA
} from "../utils/consts";
import {getIdForEmbedded} from "../utils/parser";
import buildTableActions from "./tableActions";

/// Common
//==================================================================

const day = (text) => moment(text).format("YYYY-MM-DD");
const time = (text) => {
    let time = moment(text);
    return time.format('HH:mm')
};

/// Generator
//==================================================================

const genericColumn = (name, alphabetic) => {

    let lower = name.toString().toLowerCase();

    const alpha = (a, b) => {
        return a[lower].toString()
            .localeCompare(b[lower].toString())
    };

    const num = (a, b) => {
        return a[lower] - b[lower];
    };

    return {
        title: name,
        dataIndex: lower,
        sorter: alphabetic ? alpha : num,
        render: (text) => text === null ? alphabetic ? "NULL" : 0 : text
    }

};

const genericColumnBool = (name) => {

    let lower = name.toString().toLowerCase();

    return {
        title: name,
        dataIndex: lower,
        render: (text) => text ?
            <Icon type="check-circle-o"/> : <Icon type="close-circle-o"/>,
        filters: [
            {text: 'Confirmed', value: true},
            {text: 'Unconfirmed', value: false},
        ],
        onFilter: (value, record) => {
            return record[lower] === value
        },
    }
};

const genericColumnDate = (name) => {

    let lower = name.toString().toLowerCase();

    const alpha = (a, b) => {

        return a[lower].toString()
            .localeCompare(b[lower].toString())
    };

    return {
        title: name,
        dataIndex: lower,
        render: day,
        sorter: alpha,
    }
};

const columnId = () => {

    return {
        title: 'Id',
        dataIndex: '',
        render: (text) => {
            return getIdForEmbedded(text);
        },
        sorter: (a, b) => parseInt(getIdForEmbedded(a), 10) -
            parseInt(getIdForEmbedded(b), 10),
    };
};


/// COLUMNS
//==================================================================

export const RESERVATION_COLUMNS = [
        columnId(),
        genericColumnDate("Date"),
        {
            title: "Start Time",
            dataIndex: "startTime",
            sorter: (a, b) => a.startTime.toString()
                .localeCompare(b.startTime.toString()),
            render: time,

        },
        {
            title: "End Time",
            dataIndex:
                "endTime",
            sorter:
                (a, b) => a.endTime.toString()
                    .localeCompare(b.endTime.toString()),
            render: time
        }
        ,
        genericColumn("Price", false),
        genericColumnBool("Confirmed"),
        {
            title: "Owner",
            dataIndex: "user",
            render: (text) => {
                return getIdForEmbedded(text)
            },
            sorter: (a, b) => parseInt(getIdForEmbedded(a.user), 10) -
                parseInt(getIdForEmbedded(b.user), 10),
        },
        {
            title: "Restaurant",
            dataIndex: "restaurant",
            render: (text) => {
                return getIdForEmbedded(text)
            },
            sorter: (a, b) => parseInt(getIdForEmbedded(a.restaurant), 10) -
                parseInt(getIdForEmbedded(b.restaurant), 10),
        },
        buildTableActions(RESERVATION_DATA)
    ]
;

export const USER_COLUMNS = [
    columnId(),
    genericColumn("Name", true),
    genericColumn("Email", true),
    genericColumn("Phone", true),
    genericColumn("Username", true),
    genericColumnBool("Enabled"),
    {
        title: "Last Password Update",
        dataIndex: "lastPasswordUpdate",
        sorter: (a, b) => a.lastPasswordUpdate.toString()
            .localeCompare(b.lastPasswordUpdate.toString()),
        render: (text) => moment(text).format()
    },
    buildTableActions(USER_DATA)
];
export const ROLE_COLUMNS = [
    columnId(),
    genericColumn("Name", true),
    buildTableActions(ROLE_DATA)
];
export const SIMPLE_USER_COLUMNS = [
    columnId(),
    genericColumn("Name", true),
    genericColumn("Email", true),
    genericColumn("Phone", true),
    buildTableActions(SIMPLE_USER_DATA)
];

export const PLANNING_COLUMNS = [
    columnId(),
    {
        title: "Opening Time",
        dataIndex: "openingTime",
        sorter: (a, b) => a.openingTime.toString()
            .localeCompare(b.openingTime.toString()),
        render: time
    },
    {
        title: "Closing Time",
        dataIndex: "closingTime",
        sorter: (a, b) => a.closingTime.toString()
            .localeCompare(b.closingTime.toString()),
        render: time,
    },
    buildTableActions(PLANNING_DATA),
];
export const SPECIAL_DAY_COLUMNS = [
    columnId(),
    {
        title: "Date",
        dataIndex: "date",
        sorter: (a, b) => a.date.toString()
            .localeCompare(b.date.toString()),
        render: day
    },
    {
        title: "Opening Time",
        dataIndex: "openingTime",
        sorter: (a, b) => a.openingTime.toString()
            .localeCompare(b.openingTime.toString()),
        render: (text, record) => record['workingDay'] ? text : '-'
    },
    {
        title: "Closing Time",
        dataIndex: "closingTime",
        sorter: (a, b) => a['closingTime'].toString()
            .localeCompare(b['closingTime'].toString()),
        render: (text, record) => record['workingDay'] ? text : "-"
    },
    {
        title: "Working Day",
        dataIndex: "workingDay",
        sorter: (a, b) => a['workingDay'].toString()
            .localeCompare(b['workingDay'].toString()),
        render: (text) => text ?
            <Icon type="check-circle-o"/> : <Icon type="close-circle-o"/>,
        filters: [
            {text: 'Working Day', value: true},
            {text: 'Day Off', value: false},
        ],
        onFilter: (value, record) => {
            return record['workingDay'] === value
        },
    },
    buildTableActions(SPECIAL_DAY_DATA)
];

export const DEFAULT_DISCOUNT_COLUMNS = [
    columnId(),
    genericColumn("Ratio", true),
    genericColumn("Reduction", true),
    buildTableActions(DEFAULT_DISCOUNT_DATA)
];
export const FIXED_DISCOUNT_COLUMNS = [
    columnId(),
    {
        title: "Start",
        dataIndex: "start",
        sorter: (a, b) => a.start.toString()
            .localeCompare(b.start.toString()),
        render: day
    },
    {
        title: "End",
        dataIndex: "end",
        sorter: (a, b) => a.end.toString()
            .localeCompare(b.end.toString()),
        render: day
    },
    genericColumn("Reduction", true),
    buildTableActions(FIXED_DISCOUNT_DATA)
];
export const DYNAMIC_DISCOUNT_COLUMNS = [
    columnId(),
    {
        title: "Start",
        dataIndex: "start",
        sorter: (a, b) => a.start.toString()
            .localeCompare(b.start.toString()),
        render: day
    },
    {
        title: "End",
        dataIndex: "end",
        sorter: (a, b) => a.end.toString()
            .localeCompare(b.end.toString()),
        render: day
    },
    genericColumn("Ratio", true),
    buildTableActions(DYNAMIC_DISCOUNT_DATA)
];

export const RESTAURANT_COLUMNS = [
    columnId(),
    genericColumn("Address", true),
    genericColumn("Longitude", false),
    genericColumn("Latitude", false),
    buildTableActions(RESTAURANT_DATA)
];
export const TABLE_COLUMNS = [
    columnId(),
    {
        title: "Maximum Cap.",
        dataIndex: "maxCapacity",
        sorter: (a, b) => parseInt(a.maxCapacity, 10) - parseInt(b.maxCapacity, 10)
    },
    {
        title: "Minimum Cap.",
        dataIndex: "minCapacity",
        sorter: (a, b) => parseInt(a.minCapacity, 10) - parseInt(b.minCapacity, 10)
    },
    {
        title: "Restaurant",
        dataIndex: "restaurant",
        render: (text) => {
            return getIdForEmbedded(text)
        },
        sorter: (a, b) => parseInt(getIdForEmbedded(a.restaurant), 10) -
            parseInt(getIdForEmbedded(b.restaurant), 10),
    },
    buildTableActions(TABLE_DATA)
];
export const MEAL_COLUMNS = [
    columnId(),
    genericColumn("Name", true),
    genericColumn("Type", true),
    genericColumn("Price", false),
    buildTableActions(MEAL_DATA),
];
