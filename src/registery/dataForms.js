import React from "react";
import {Checkbox, DatePicker, Form, Input, Select, TimePicker} from "antd";
import {getIdForEmbedded} from "../utils/parser";
import {RESTAURANT_DATA, SIMPLE_USER_DATA} from "../utils/consts";


const FormItem = Form.Item;
const Option = Select.Option;
const {TextArea} = Input;
const timeFormat = 'HH:mm';

export function buildSelectForm(getFieldDecorator, select, content) {

    let finalResult = content[select.data];

    return <FormItem label={select.name}>
        {getFieldDecorator(select.name.toString().toLowerCase(), {
            rules: [{required: true,}],
        })(
            <Select placeholder={"Select an option"}>
                {finalResult.map(row => {
                    return <Option value={row._links.self.href}>{getIdForEmbedded(row)}</Option>
                })}
            </Select>
        )}
    </FormItem>
}

//====================================================================

export const mealForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Name"}>
                {getFieldDecorator('name', {
                    rules: [{required: true,}],
                })(<Input placeholder="Meal name"/>)}
            </FormItem>,

            <FormItem label={"Type"}>
                {getFieldDecorator('type', {
                    rules: [{required: true,}],
                })(
                    <Select placeholder={"Meal type"}>
                        <Option value="STARTER">Starter</Option>
                        <Option value="MAIN">Main</Option>
                        <Option value="DESERT">Desert</Option>
                    </Select>
                )}
            </FormItem>,

            <FormItem label={"Price"}>
                {getFieldDecorator('price', {
                    rules: [{
                        type: 'number',
                        required: true,
                        transform: (value) => {
                            return parseInt(value, 10)
                        }
                    }],
                })(<Input placeholder={"Meal Price"}/>)}
            </FormItem>,

            <FormItem label={"Description"}>
                {getFieldDecorator('description', {
                    rules: [{required: true,}],
                })(<TextArea/>)}
            </FormItem>
        ],
    }
};

export const restaurantForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Address"}>
                {getFieldDecorator('address', {
                    rules: [{required: true,}],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Latitude"}>
                {getFieldDecorator('latitude', {
                    rules: [{
                        type: 'float',
                        required: true,
                        transform: (value) => {
                            return parseFloat(value)
                        }
                    },],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Longitude"}>
                {getFieldDecorator('longitude', {
                    rules: [{
                        type: 'float',
                        required: true,
                        transform: (value) => {
                            return parseFloat(value)
                        }
                    },],
                })(<Input/>)}
            </FormItem>
        ],
    }
};

export function tableForm(getFieldDecorator) {

    return {
        fields: [
            <FormItem label={"Max number of places"}>
                {getFieldDecorator('maxCapacity', {
                    rules: [{
                        type: 'number',
                        required: true,
                        transform: (value) => {
                            return parseInt(value, 10)
                        }
                    },],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Min number of places"}>
                {getFieldDecorator('minCapacity', {
                    rules: [{
                        type: 'number',
                        required: true,
                        transform: (value) => {
                            return parseInt(value, 10)
                        },
                    },],
                })(<Input type={"textarea"}/>)}
            </FormItem>,
        ], selects: [
            {name: 'Restaurant', data: RESTAURANT_DATA}
        ]
    };
}

//====================================================================

export const roleForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Role name"}>
                {getFieldDecorator('name', {
                    rules: [{required: true,}],
                })(<Input/>)}
            </FormItem>
        ],
    }
};

export const userForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Name"}>
                {getFieldDecorator('name', {
                    rules: [{required: true,}],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Email"}>
                {getFieldDecorator('email', {
                    rules: [{required: true,}, {type: 'email'}],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Phone"}>
                {getFieldDecorator('phone', {
                    rules: [{required: true,},],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Username"}>
                {getFieldDecorator('username', {
                    rules: [{required: true,}],
                })(<Input/>)}
            </FormItem>,


            <FormItem label={"Password"}>
                {getFieldDecorator('password', {
                    rules: [{}],
                })(<Input/>)}
            </FormItem>,


            <FormItem label={"Enabled"}>
                {getFieldDecorator('enabled', {
                    rules: [{required: true,}],
                })(<Checkbox>Enabled</Checkbox>)}
            </FormItem>
        ]
    };
};

export const simpleUserForm = (getFieldDecorator) => {
    return {
        fields: [

            <FormItem label={"Name"}>
                {getFieldDecorator('name', {
                    rules: [{required: true,}],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Email"}>
                {getFieldDecorator('email', {
                    rules: [{required: true,}, {type: 'email'}],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Phone"}>
                {getFieldDecorator('phone', {
                    rules: [{required: true,},],
                })(<Input/>)}
            </FormItem>
        ]
    };

};

//====================================================================

export const fixedDiscountForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Start"}>
                {getFieldDecorator('start', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,

            <FormItem label={"End"}>
                {getFieldDecorator('end', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,


            <FormItem label={"Reduction"}>
                {getFieldDecorator('reduction', {
                    rules: [{
                        type: 'number',
                        required: true,
                        transform: (value) => {
                            return parseInt(value, 10)
                        },
                    },],
                })(<Input/>)}
            </FormItem>
        ]
    };
};

export const dynamicDiscountForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Start"}>
                {getFieldDecorator('start', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,

            <FormItem label={"End"}>
                {getFieldDecorator('end', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,


            <FormItem label={"Ratio"}>
                {getFieldDecorator('ratio', {
                    rules: [{
                        type: 'float',
                        required: true,
                        transform: (value) => {
                            return parseFloat(value)
                        },
                    },],
                })(<Input/>)}
            </FormItem>
        ]
    };
};

export const defaultDiscountForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Ratio"}>
                {getFieldDecorator('ratio', {
                    rules: [{
                        type: 'float',
                        transform: (value) => {
                            return parseFloat(value)
                        }
                    }],
                })(<Input/>)}
            </FormItem>,


            <FormItem label={"Reduction"}>
                {getFieldDecorator('reduction', {
                    rules: [{
                        type: 'number',
                        transform: (value) => {
                            return parseInt(value, 10)
                        }
                    }],
                })(<Input/>)}
            </FormItem>
        ]
    };
};

//====================================================================

export const planningForm = (getFieldDecorator) => {
    return {
        fields: [
            <FormItem label={"Opening Time"}>
                {getFieldDecorator('openingTime', {
                    rules: [{required: true,}],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>,

            <FormItem label={"Closing Time"}>
                {getFieldDecorator('closingTime', {
                    rules: [{required: true,}],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>
        ]
    };
};

export const specialDayForm = (getFieldDecorator) => {
    return {
        fields: [

            <FormItem label={"Opening Time"}>
                {getFieldDecorator('openingTime', {
                    rules: [],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>,

            <FormItem label={"Closing Time"}>
                {getFieldDecorator('ClosingTime', {
                    rules: [],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>,

            <FormItem label={"Date"}>
                {getFieldDecorator('date', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,

            <FormItem label={"WorkingDay"}>
                {getFieldDecorator('workingDay', {
                    rules: [{required: true,}],
                })(<Checkbox>Working Day</Checkbox>)}
            </FormItem>
        ]
    };
};

//====================================================================

export function reservationForm(getFieldDecorator) {

    return {
        fields: [
            <FormItem label={"Date"}>
                {getFieldDecorator('date', {
                    rules: [{required: true,}],
                })(<DatePicker/>)}
            </FormItem>,

            <FormItem label={"Start Time"}>
                {getFieldDecorator('startTime', {
                    rules: [{required: true,}],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>,

            <FormItem label={"End Time"}>
                {getFieldDecorator('endTime', {
                    rules: [{required: true,}],
                })(<TimePicker format={timeFormat}/>)}
            </FormItem>,

            <FormItem label={"Price"}>
                {getFieldDecorator('price', {
                    rules: [{
                        type: 'number',
                        transform: (value) => {
                            return parseInt(value, 10)
                        }
                    }],
                })(<Input/>)}
            </FormItem>,

            <FormItem label={"Confirmed"}>
                {getFieldDecorator('confirmed', {
                    rules: [],
                })(<Checkbox>Confirmed</Checkbox>)}
            </FormItem>,

        ], selects: [
            {name: 'User', data: SIMPLE_USER_DATA},
            {name: 'Restaurant', data: RESTAURANT_DATA}
        ]
    };
}
