import React from 'react';
import Buttons from "../components/content/sub-panels/data/sub-components/table/table-holders/TableButtons";

const buildTableActions = (type) => {

    return {
        title: 'Action', dataIndex: '', key: 'x', render: (text,record,index) =>
            <Buttons type={type} record={record}/>,
        align: 'center',
    };
};

export default buildTableActions;
