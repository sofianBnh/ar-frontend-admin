export function getIdForEmbedded(row) {
    const route = row._links.self.href.toString();
    return route.substring(route.lastIndexOf("/") + 1, route.length)
}

