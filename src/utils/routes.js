export const LOGIN_ROUTE = '/auth';
export const ADMIN_CHECK_ROUTE = '/admin';


export const RESERVATION_ROUTE = '/reservations';

export const DEFAULT_DISCOUNT_ROUTE = '/defaultDiscounts';
export const DYNAMIC_DISCOUNT_ROUTE = '/dynamicDiscounts';
export const FIXED_DISCOUNT_ROUTE = '/fixedDiscounts';

export const PLANNING_ROUTE = '/defaultPlannings';
export const SPECIAL_DAY_ROUTE = '/specialDays';

export const ROLE_ROUTE = '/roles';
export const SIMPLE_USER_ROUTE = '/simpleUsers';
export const USER_ROUTE = '/users';

export const RESTAURANT_ROUTE = '/restaurants';
export const TABLE_ROUTE = '/restaurantTables';
export const MEAL_ROUTE = '/meals';
