import {TOKEN_BEARER, TOKEN_COOKIE_KEY} from "./consts";
import * as cookies from "react-cookies";


export const getAuthHeader = () => {
    const token = cookies.load(TOKEN_COOKIE_KEY);

    return {
        Authorization: TOKEN_BEARER + token
    };
};


export function getErrorFromResponse(error) {
    let errorText = error.response.data;

    if (errorText.message)
        errorText = errorText.message;
    return errorText;
}


