/// Data types
//==================================================================

export const RESERVATION_DATA = 'RESERVATION_DATA';

// discount
export const DEFAULT_DISCOUNT_DATA = 'DEFAULT_DISCOUNT_DATA';
export const DYNAMIC_DISCOUNT_DATA = 'DYNAMIC_DISCOUNT_DATA';
export const FIXED_DISCOUNT_DATA = 'FIXED_DISCOUNT_DATA';

// planning
export const SPECIAL_DAY_DATA = 'SPECIAL_DAY_DATA';
export const PLANNING_DATA = 'PLANNING_DATA';

// restaurant
export const RESTAURANT_DATA = 'RESTAURANT_DATA';
export const TABLE_DATA = 'TABLE_DATA';
export const MEAL_DATA = 'MEAL_DATA';

// users
export const SIMPLE_USER_DATA = 'SIMPLE_USER_DATA';
export const USER_DATA = 'USER_DATA';
export const ROLE_DATA = 'ROLE_DATA';

/// Data contents
//==================================================================

export const RESERVATION_DATA_CONTENT = 'RESERVATION_DATA_CONTENT';
export const RESTAURANT_DATA_CONTENT = 'RESTAURANT_DATA_CONTENT';
export const USER_DATA_CONTENT = 'USER_DATA_CONTENT';

export const ADMINISTRATION_DATA_CONTENT = 'ADMINISTRATION_DATA_CONTENT';

export const DISCOUNT_DATA_CONTENT = 'DISCOUNT_DATA_CONTENT';
export const PLANNING_DATA_CONTENT = 'PLANNING_DATA_CONTENT';


/// Menus
//==================================================================

export const DATA_MENU_KEY = 'DATA_MENU_KEY';
export const CHART_MENU_KEY = 'CHART_MENU_KEY';

export const DATA_MENU = {
    name: "Data",
    icon: "database",
    key: DATA_MENU_KEY,
    sub: [
        {
            name: "Reservations",
            key: RESERVATION_DATA_CONTENT,
            icon: "schedule",
            parts: [{name: 'Reservations', key: RESERVATION_DATA},],
        },
        {
            name: "Restaurants",
            key: RESTAURANT_DATA_CONTENT,
            icon: "home",
            parts: [
                {name: 'Restaurants', key: RESTAURANT_DATA},
                {name: 'Tables', key: TABLE_DATA},
                {name: 'Meals', key: MEAL_DATA},
            ]
        },
        {
            name: "Discount",
            key: DISCOUNT_DATA_CONTENT,
            icon: "tags-o",
            sub: [
                {name: 'Default Disc.', key: DEFAULT_DISCOUNT_DATA},
                {name: 'Dynamic Disc.', key: DYNAMIC_DISCOUNT_DATA},
                {name: 'Fixed Disc.', key: FIXED_DISCOUNT_DATA},
            ]
        },
        {
            name: "Planning",
            key: PLANNING_DATA_CONTENT,
            icon: "calendar",
            sub: [
                {name: 'Plannings', key: PLANNING_DATA},
                {name: 'Special Days', key: SPECIAL_DAY_DATA},
            ]
        },
        {
            name: "Users",
            key: USER_DATA_CONTENT,
            icon: "user",
            parts: [
                {name: 'Registered', key: USER_DATA},
                {name: 'Simple', key: SIMPLE_USER_DATA},
                {name: 'Role', key: ROLE_DATA},
            ],
        }
    ],
};
export const CHART_MENU = {
    name: "Dashboard",
    icon: "pie-chart",
    key: CHART_MENU_KEY,
};


/// CONFIG
//==================================================================

export const RESTAURANT_NAME = 'Abraham & Russo';
export const TOKEN_COOKIE_KEY = 'TOKEN_COOKIE_KEY';
export const TOKEN_BEARER = 'Bearer ';


/// OPERATIONS
//==================================================================

export const EDIT = 'EDIT';
export const ADD = 'ADD';