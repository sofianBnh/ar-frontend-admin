import {Menu} from "antd/lib/index";
import {Icon} from "antd";
import React from "react";

const {SubMenu} = Menu;

export const buildMenu = (menu) => {

    if (menu.sub) {
        return <SubMenu
            key={menu.key}
            title={<span>
                {menu.icon && <Icon type={menu.icon}/>}
                <span>{menu.name}</span>
                </span>}>
            {menu.sub.map(subMenu => {
                return buildMenu(subMenu)
            })}
        </SubMenu>
    }

    if (Array.isArray(menu.parts)) {
        return <SubMenu
            key={menu.key}
            title={<span>
                {menu.icon && <Icon type={menu.icon}/>}
                <span>{menu.name}</span>
                </span>}>
            {menu.parts.map(subMenu => {
                return buildMenu(subMenu)
            })}
        </SubMenu>
    }

    return <Menu.Item key={menu.key}>
        {menu.icon && <Icon type={menu.icon}/>}
        <span>{menu.name}</span>
    </Menu.Item>
};

